﻿//Henry Smith

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class BulletPattern : MonoBehaviour
{
    [SerializeField]
    private List<string> PoolNames;

    [SerializeField]
    public float delay = 0.1f;

    protected List<ObjectPool> Pools = new List<ObjectPool>();

    private void Awake()
    {
        foreach(string name in PoolNames)
        {
            Pools.Add(GameObject.Find(name).GetComponent<ObjectPool>());
        }
    }

    public virtual void Shoot()
    {
        Pools[0].GetObject(transform.position, transform.eulerAngles.z);
    }
}
