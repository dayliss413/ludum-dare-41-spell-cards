﻿//Henry Smith

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class RadialForward : BulletPattern
{
    [SerializeField]
    [MinValue(2)]
    private int Count = 2;

    [SerializeField]
    private float minAngle = -30;

    [SerializeField]
    private float maxAngle = 30;

    public override void Shoot()
    {
        for(int i = 0; i < Count; ++i)
        {
            ForwardBullet bullet = Pools[0].GetObject(transform.position, 0).GetComponent<ForwardBullet>();
            bullet.rotation = Mathf.Lerp(minAngle, maxAngle, ((float)i / (Count - 1)))  + transform.eulerAngles.z;
        }
    }
}
