﻿//Henry Smith

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class Bullet : MonoBehaviour
{
    [SerializeField]
    private bool DeathTime;

    [ShowIf("DeathTime")]
    [SerializeField]
    private float Duration = 10;

    private float CurrentTime = 0;
    protected Rigidbody2D Body;
    protected SpriteRenderer View;
    protected Collider2D Hitbox;

    private void Awake()
    {
        Body = GetComponent<Rigidbody2D>();
        View = GetComponentInChildren<SpriteRenderer>();
        Hitbox = GetComponentInChildren<Collider2D>();
    }

    private void OnEnable()
    {
        if(DeathTime)
            StartCoroutine(WaitThenDie());
    }

    private IEnumerator WaitThenDie()
    {
        yield return new WaitForSeconds(Duration);
        gameObject.SetActive(false);
    }

    private void FixedUpdate()
    {
        CurrentTime += Time.fixedDeltaTime;
        Move(CurrentTime, Time.fixedDeltaTime);
    }

    protected abstract void Move(float Time, float DeltaTime);
}
