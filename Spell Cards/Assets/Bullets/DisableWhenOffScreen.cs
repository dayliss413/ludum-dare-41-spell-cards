﻿//Henry Smith

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class DisableWhenOffScreen : MonoBehaviour
{
    protected virtual void OnTriggerExit2D(Collider2D collider)
    {
        if(collider.tag == "PlaySpace")
        {
            gameObject.SetActive(false);
        }
    }
}
