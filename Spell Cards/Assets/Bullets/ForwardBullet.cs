﻿//Henry Smith

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ForwardBullet : Bullet
{
    public Vector2 Velocity;

    public float rotation = 0;

    private void OnEnable()
    {
        rotation = 0;
    }

    protected override void Move(float Time, float DeltaTime)
    {
        Body.position += Velocity.Rotate(rotation + transform.eulerAngles.z) * DeltaTime;
    }
}
