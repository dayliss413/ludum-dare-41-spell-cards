﻿//Henry Smith

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;

public class ObjectPool : MonoBehaviour
{
    [SerializeField]
    [AssetsOnly]
    private GameObject PooledObject;

    [SerializeField]
    private int ToPreMake;

    private List<GameObject> Pooled = new List<GameObject>();

    private void Awake()
    {
        for(int i = 0; i < ToPreMake; ++i)
        {
            CreateObject();
        }
    }

    private GameObject CreateObject()
    {
        GameObject NewObject = Instantiate(PooledObject);
        
        Pooled.Add(NewObject);
        NewObject.transform.parent = transform;
        NewObject.SetActive(false);

        return NewObject;
    }

    public GameObject GetObject(Vector2 Position, float Rotation)
    {
        GameObject ToReturn = Pooled.Find(x => x.activeInHierarchy == false);
        if(ToReturn == null)
        {
            ToReturn = CreateObject();
        }

        ToReturn.transform.position = Position;
        ToReturn.transform.eulerAngles = new Vector3(0, 0, Rotation);
        ToReturn.SetActive(true);

        return ToReturn;
    }
}
