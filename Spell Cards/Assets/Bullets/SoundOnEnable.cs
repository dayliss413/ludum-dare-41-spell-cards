﻿//Henry Smith

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class SoundOnEnable : MonoBehaviour
{
    [SerializeField]
    private AudioClip Clip;

    [SerializeField]
    private string SourcePoolName;
    
    private ObjectPool SourcePool;
    private AudioSource Source;
    private void Awake()
    {
        SourcePool = GameObject.Find(SourcePoolName).GetComponent<ObjectPool>();
    }

    private void OnEnable()
    {
        StartCoroutine(PlayAudio());
    }

    private void OnDisable()
    {
        if(Source)
        {
            Source.Stop();
            Source.gameObject.SetActive(false);
        }
    }

    private IEnumerator PlayAudio()
    {
        Source = SourcePool.GetObject(Vector3.zero, 0).GetComponent<AudioSource>();
        Source.PlayOneShot(Clip);
        yield return new WaitForSeconds(Clip.length);
        Source.gameObject.SetActive(false);
    }

}
