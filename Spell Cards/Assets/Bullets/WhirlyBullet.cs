﻿//Henry Smith

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class WhirlyBullet : Bullet
{
    public float theta = 0;
    public float radius = 1;
    public float angularVelocity = 3;

    private void OnEnable()
    {
        theta = 0;
        Body.position += new Vector2(1, 0) * radius;
    }

    protected override void Move(float Time, float DeltaTime)
    {
        Body.position -= new Vector2(Mathf.Cos(theta) - 2, Mathf.Sin(theta)) * radius;
        theta += DeltaTime * angularVelocity;
        Body.position += new Vector2(Mathf.Cos(theta) - 2, Mathf.Sin(theta)) * radius;
    }
}
