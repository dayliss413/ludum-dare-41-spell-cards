﻿//Henry Smith

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class PlayerMove : MonoBehaviour
{
    public float Speed;

    private Rigidbody2D Body;

    private void Awake()
    {
        Body = GetComponent<Rigidbody2D>();
    }

    private void MoveUp()
    {
        
    }

    private void FixedUpdate()
    {
        float HorizontalMove = Input.GetAxisRaw("Horizontal") * Speed * Time.fixedDeltaTime;
        float VerticalMove = Input.GetAxisRaw("Vertical") * Speed * Time.fixedDeltaTime;

        Body.position +=  new Vector2(HorizontalMove, VerticalMove);
    }
}
