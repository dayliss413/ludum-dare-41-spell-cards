﻿//Henry Smith

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class PlayerShoot : MonoBehaviour
{
    [SerializeField]
    [SceneObjectsOnly]
    [ListDrawerSettings(AlwaysAddDefaultValue = true)]
    private List<BulletPattern> Patterns;

    private float CurrentTime = 0;

    public int CurrentPattern = 3;

    public void FixedUpdate()
    {
        if(CurrentTime <= 0)
        {
            if(Input.GetKey(KeyCode.Space))
            {
                Patterns[CurrentPattern].Shoot();
                CurrentTime = Patterns[CurrentPattern].delay;
            }
        }
        else
        {
            CurrentTime -= Time.deltaTime;
        }
    }

    public void ChangeFireMode()
    {

    }
}
